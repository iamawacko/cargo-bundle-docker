FROM rust:stretch

RUN rustup toolchain install 1.62.0
RUN rustup default 1.62
RUN rustc --version && cargo --version

RUN cargo install cargo-bundle

WORKDIR /home/
